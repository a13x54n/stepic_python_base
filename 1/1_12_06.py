n = int(input())
if n == 0 or n % 10 == 0 or 4 < n < 19 or 4 < n % 10 or 10 < n % 100 <20:
    print(n, 'программистов')
elif 1 < n < 5 or 1 < n % 10 < 5:
    print(n, 'программиста')
elif n == 1 or n % 10 == 1:
    print(n, 'программист')