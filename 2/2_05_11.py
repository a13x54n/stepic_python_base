a = [int(i) for i in input().split()]
a.sort()
s = []
for i in range((len(a) - 1)):
    for j in range(i + 1, (len(a))):
        if len(s) > 0 and a[i] == s[-1]:
            break
        if a[i] == a[j] and s.count(str(a[i])) < 1:
            s.append(a[i])
            break
for i in s:
    print(i, end=' ')
