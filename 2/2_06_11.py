k = n = int(input())
m = [[0] * n for i in range(n)]
x = y = 0
z = 1
m[y][x] = z
while z < n * n:
    while x < (k - 1):
        m[y][x] = z
        z += 1
        x += 1
    while y < (k - 1):
        m[y][x] = z
        z += 1
        y += 1
    while x > (n - k):
        m[y][x] = z
        z += 1
        x -= 1
    while y > (n - k):
        m[y][x] = z
        z += 1
        y -= 1
    x += 1
    y += 1
    k -= 1
    if 0 < (n // 2) == (k - 1):
        m[y][x] = z
for i in range(n):
    for j in range(n):
        print(m[i][j], end='\t')
    print()
