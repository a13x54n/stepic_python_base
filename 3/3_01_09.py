def modify_list(l):
    i = 0
    while i < len(l):
        if l[i] % 2 == 1:
            l.remove(l[i])
        else:
            i += 1
    for i in range(len(l)):
        l[i] = int(l[i] / 2)
