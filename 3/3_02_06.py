d = {}
for w in input().lower().split():
    if w in d:
        d[w] += 1
    else:
        d[w] = 1
for w in d:
    print(w, d[w])
