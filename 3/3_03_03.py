d = {}
mw = ['', 0]
for l in (open('dataset_3363_3.txt', 'r')):
    for s in l.strip().lower().split():
        if s not in d:
            d[s] = 0
        d[s] += 1
        if (d[s] > mw[1]) or ((d[s] == mw[1]) and (s < mw[0])) or (mw[0] == ''):
            mw = [s, d[s]]
print(mw[0], ' ', mw[1], sep='', end='')
