from datetime import datetime

import requests

start = datetime.now()
url = (open('dataset_3378_3.txt', 'r').readline()).strip()
print(url)
w = ''
c = 0
while w != 'We':
    r = requests.get(url).text.strip()
    if 'We' in r:
        print(r)
        w = 'We'
    else:
        url = 'https://stepic.org/media/attachments/course67/3.6.3/' + r
        print(url)
        c += 1
print(c)

# (requests.get(url).text)
# t = r.text
# print(len(t.splitlines()))
# print(len(requests.get(open('dataset_3378_2.txt', 'r').readline().strip()).text.splitlines()))
print(datetime.now() - start)
