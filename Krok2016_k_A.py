from datetime import datetime

n, a, b = map(int, (input().split()))
start = datetime.now()
if n > (a * b):
    print(-1)
else:
    for i in range(a):
        if (b % 2 == 0) and ((i + 1) % 2 == 0):
            for c in range(b * (i + 1), b * (i + 1) - b, -1):
                if c <= n:
                    print(c, end=' ')
                else:
                    print(0, end=' ')
        else:
            for c in range((b * (i + 1) - b) + 1, b * (i + 1) + 1):
                if c <= n:
                    print(c, end=' ')
                else:
                    print(0, end=' ')
            print()
print(datetime.now() - start)
