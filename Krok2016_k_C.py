n = int(input())
d = {}
for i in range(n):
    s = input() + ' '
    a = s[:(s.find('/', 7))]
    p = s[(s.find('/', 7)):]
    if a in d:
        if p not in d.get(a):
            d[a] += [p]
    else:
        d[a] = [p]
k = []
for i in d:
    d[i] = sorted(d.get(i))
    k += [i]
t = 0
o = []
while len(k) > t:
    r = []
    for i in range(0, len(k)):
        print(d[k[t]])
        if d[k[t]] == d[k[i]]:
            if k[t] not in r:
                r += k[t]
            r += ' ' + k[i]

    t += 1
    o += [r]
print(len(o))
for i in o:
    print(i)

# list.remove(x)	Удаляет первый элемент в списке, имеющий значение x
# list.pop([i])	Удаляет i-ый элемент и возвращает его. Если индекс не указан, удаляется последний элемент
# k = []
# k = d.keys()
# print(k)
#     k+=1
#     for i in d:
#         if d[k] == d[i]:
#             print(d[i])



# S.find(str, [start],[end])	Поиск подстроки в строке. Возвращает номер первого вхождения или -1
# S.endswith(str)	Заканчивается ли строка S шаблоном str
